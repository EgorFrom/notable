import 'react-native-gesture-handler';
import React from 'react';
import {
  StyleSheet,
  StatusBar,
  Text,
} from 'react-native';
import { Provider as ReduxProvider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from './styles/colors';
import TeamsStackScreen from './stacks/Teams/TeamsStackScreen';
import ProfileStackScreen from './stacks/Profile/ProfileStackScreen';
import MessengerStackScreen from './stacks/Messenger/MessengerStackScreen';
import ReportsStackScreen from './stacks/Reports/ReportsStackScreen';
import SettingsStackScreen from './stacks/Settings/SettingsStackScreen';
import configureStore, { StoreManager } from './store/configure';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: colors.primary,
    accent: colors.accent,
  },
};

const Tab = createBottomTabNavigator();

const tabs = [
  { component: TeamsStackScreen, name: 'Teams', tabIconName: 'groups'},
  { component: ProfileStackScreen, name: 'Profile', tabIconName: 'person'},
  { component: MessengerStackScreen, name: 'Messenger', tabIconName: 'send'},
  { component: ReportsStackScreen, name: 'Reports', tabIconName: 'leaderboard'},
  { component: SettingsStackScreen, name: 'Settings', tabIconName: 'settings'}
];

const { store, persistor } = configureStore();
const storeManager = StoreManager.getInstance();
storeManager.setStore(store);
storeManager.setPersistor(persistor);

const App = () => {
  return (
    <ReduxProvider store={store}>
      <PersistGate persistor={persistor}>
        <PaperProvider theme={theme}>
          <StatusBar barStyle={"light-content"} />
          <NavigationContainer>
            <Tab.Navigator
              screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, size }) => {
                  const currentScreen = tabs.find(tab => tab.name === route.name);
                  return <Icon name={currentScreen.tabIconName} size={size} color={focused ? colors.dark : colors.primary} />;
                },
                tabBarLabel: ({ focused }) => (<Text style={{ color: focused ? colors.dark : colors.primary}}>{route.name}</Text>),
              })}
            >
              {tabs.map(tab => (
                <Tab.Screen key={tab.name} name={tab.name} component={tab.component} />
              ))}
            </Tab.Navigator>
          </NavigationContainer>
        </PaperProvider>
      </PersistGate>
    </ReduxProvider>
  );
};

const styles = StyleSheet.create({
});

export default App;
