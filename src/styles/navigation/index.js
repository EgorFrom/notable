import colors from "../colors";

export const defaultNavigator = {
  headerStyle: {
    backgroundColor: colors.primary,
  },
  headerTintColor: colors.white,
};

export const hiddenNavigator = {
  hidden: true,
  headerShown: false,
};