const colors = {
  accent: '#FF875C',
  primary: '#3D7CA4',
  secondary: '#7A1524',
  light: '#ABDAE1',
  dark: '#272838',
  white: '#fff',
  background: '#f2f2f2'
};

export const allowedColors = {
  desertSand: '#E4BB97',
  periwinkleCrayola: '#D6E3F8',
  cooperRose: '#9D5C63',
  darkLiver: '#584B53',
  mistyMoss: '#C2B97F',
  sugarPlum: '#8E5572',
  winterGreenDream: '#668F80',
  pastelPink: '#D6A2AD',
  skobeloff: '#197278',
  slimyGreen: '#379634',
  aquamarine: '#7CFFCB',
};

export default colors;