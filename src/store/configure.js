/* global __DEV__ */
import { applyMiddleware, createStore } from 'redux';
import { persistStore } from 'redux-persist';
import createSagaMiddleware from 'redux-saga';
import reducers from './reducers';
import rootSaga from '../modules/rootSaga';

export class StoreManager {
    static instance = null;

    store = {};

    persistor = {};

    static getInstance() {
      if (StoreManager.instance == null) {
        StoreManager.instance = new StoreManager();
      }

      return this.instance;
    }

    getStore() {
      return this.store;
    }

    getPersistor() {
      return this.persistor;
    }

    setStore(store) {
      this.store = store;
    }

    setPersistor(persistor) {
      this.persistor = persistor;
    }
}

const sagaMiddleware = createSagaMiddleware();

let middlewares = [
  sagaMiddleware,
];

function configureStore() {
  const store = createStore(reducers, applyMiddleware(...middlewares));
  const persistor = persistStore(store);

  sagaMiddleware.run(rootSaga);

  return { persistor, store };
}

export default configureStore;
