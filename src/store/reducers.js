import { persistCombineReducers } from 'redux-persist';
import { filterActions } from 'redux-ignore';
import persistConfig from './persistConfig';
import settings from '../modules/settings';

const reducers = persistCombineReducers(persistConfig, {
  settings: filterActions(settings, action => action.type.includes('~settings/')),
});

export default reducers;
