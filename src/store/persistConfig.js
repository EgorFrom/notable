import AsyncStorage from '@react-native-async-storage/async-storage';
// import { createFilter, createBlacklistFilter } from 'redux-persist-transform-filter';

const config = {
  key: 'root',
  storage: AsyncStorage,
  // whitelist: [
  // ],
  // transforms: [
  // ],
};

export default config;
