// TODO add onResponse
// TODO add onError

/* eslint no-unused-vars:0 */
const StatusHandler = {
  200: (json) => {
    return { response: json.data };
  },
  401: () => {
    return { error: 'Unauthorized' };
    // throw new Error('Unauthorized');
  },
  500: (json) => {
    return { error: `Server error ${json}` };
    // throw new Error(`Server error ${json}`);
  },
  error: (json) => {
    return { error: json };
    // throw json;
  },
};

export const mapJsonToData = (json) => {
  const code = json.status;
  const handler = StatusHandler[code];
  if (handler && json.data) {
    return handler(json);
  }
  return StatusHandler.error(json);
};

export const mapResToJson = response => response.json();

class ApiGateway {
  defaultHeaders;

  constructor() {
    this.defaultHeaders = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
    };
  }

  generateRequest(url, type, headers, body) {
    let requestOptions = {
      method: type,
      headers: new Headers(Object.assign({}, this.defaultHeaders, headers)),
    };
    if (type !== 'GET' && type !== 'HEAD') {
      requestOptions = Object.assign({}, requestOptions, { body: JSON.stringify(body) });
    }
    return new Request(url, requestOptions);
  }

  // be careful with fullResponse, it doesn't check errors
  request(url, type = 'GET', headers, body, fullResponse = false) {
    return fetch(this.generateRequest(url, type, headers, body))
      .then(mapResToJson)
      .then((json) => {
        // console.log('logger: ', {  url, type, headers, body, response: json }); // <-- logger
        if (fullResponse) return json;
        return mapJsonToData(json);
      });
  }
}

export default ApiGateway;
