// @flow
import { createReducer, createActionTypes } from 'zeal-redux-utils';

const statuses = ['REQUEST', 'SUCCESS', 'ERROR', 'CANCEL', 'RETRY'];

export { createActionTypes };

export function generateNetworkActions(prefix) {
  return statuses.map(postfix => `${prefix}_${postfix}`);
}

export function reducer(initialState, handlers) {
  return createReducer(initialState, handlers, { allowNonStandardActionIf: () => true });
}
