import Config from 'react-native-config';

class ApiQuery {
  version;

  key;

  endpoint;

  queryGet;

  queryPath;

  joinQuery;

  withQuery;

  selectQuery;

  filterQuery;

  orderQuery;

  limitQuery;

  offsetQuery;

  static API_KEY_DEV = Config.API_KEY_DEV;

  static API_KEY_PROD = Config.API_KEY;

  static endpoint = {
    v1: Config.API_URL,
    // telegram: Config.API_URL_TELEGRAM,
    // viber: Config.API_URL_VIBER,
  };

  constructor(options = {}, version = 'v1') {
    if (options.useApikeyProd) {
      this.key = ApiQuery.API_KEY_PROD;
    } else {
      this.key = __DEV__ ? ApiQuery.API_KEY_DEV : ApiQuery.API_KEY_PROD;
    }
    this.endpoint = ApiQuery.endpoint[version];
    this.queryGet = new Set(options.queryGet || []);
    this.queryPath = new Set(options.queryPath || []);
    this.joinQuery = new Set(options.joinQuery || []);
    this.withQuery = new Set(options.withQuery || []);
    this.selectQuery = new Set(options.selectQuery || []);
    this.filterQuery = new Set(options.filterQuery || []);
    this.orderQuery = new Set(options.orderQuery || []);
  }

  getQueryPath = () => this.stringQuery(this.queryPath);

  getQueryGet = () => this.stringQuery(this.queryGet, '&');

  getQuerySelect = () => this.collectionQuery(this.selectQuery, '&select=[');

  getQueryFilter = () => this.collectionQuery(this.filterQuery, '&filter=["and", ');

  getQueryJoin = () => this.collectionQuery(this.joinQuery, '&joinwith=[');

  getQueryOrder = () => this.collectionQuery(this.orderQuery, '&order_by=[');

  getQueryWith = () => this.collectionQuery(this.withQuery, '&with=[');

  stringQuery = (collection, queryPrefix = '') => {
    const path = collection.size > 0 && Array.from(collection)[0].join('/').replace('/&', '&');

    return path && path !== '' ? `${queryPrefix}${path}` : '';
  };

  collectionQuery = (collection, prefix) => {
    const filterQuery = collection.size > 0 && Array.from(collection)[0]
      .filter(item => item)
      .map((item) => JSON.stringify(item)).join(',');

    return filterQuery && filterQuery !== '' ? `${prefix}${filterQuery}]` : '';
  };

  path = (path) => {
    this.queryPath.add(path);

    return this;
  };

  get = (get) => {
    get && this.queryGet.add(get);

    return this;
  };

  select = (fields) => {
    this.selectQuery.add(fields);
    return this;
  };

  filter = (fields) => {
    this.filterQuery.add(fields);
    return this;
  };

  joinwith = (fields) => {
    this.joinQuery.add(fields);
    return this;
  };

  order = (fields) => {
    this.orderQuery.add(fields);
    return this;
  };

  with = (fields) => {
    this.withQuery.add(fields);
    return this;
  };

  limit = (number) => {
    this.limitQuery = number;

    return this;
  };

  offset = (number) => {
    this.offsetQuery = number;

    return this;
  };

  stringify = () => [
    `${this.endpoint}/`,
    this.getQueryPath(),
    `?api_key=${this.key}`,
    this.limitQuery ? `&limit=${this.limitQuery}` : '',
    this.offsetQuery ? `&offset=${this.offsetQuery}` : '',
    this.getQueryGet(),
    this.getQuerySelect(),
    this.getQueryFilter(),
    this.getQueryJoin(),
    this.getQueryWith(),
    this.getQueryOrder(),
  ].join('');
}

export default ApiQuery;
