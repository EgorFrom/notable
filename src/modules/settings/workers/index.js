import { call, put, takeEvery } from 'redux-saga/effects';
import { settingsTypes } from '..';
import {
  flushSettingsErrorAction,
  setPersonalColorAction,
  setSettingsErrorAction,
} from '../actions';
import { getSettingsRequest } from "../api/requests";

function* getSettingsWorker() {
  const { response, error } = yield call(getSettingsRequest);
  if (response) {
    yield put(flushSettingsErrorAction());
    //TODO: rewrite after api settings ready
    const personalColor = response.personalColor;
    yield put(setPersonalColorAction(personalColor));
  } else {
    yield put(setSettingsErrorAction(error));
  }
}

function* settingsWatcher() {
  yield takeEvery(settingsTypes.GET_SETTINGS, getSettingsWorker);
}

export default settingsWatcher;