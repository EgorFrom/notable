import { settingsTypes } from "..";

export const getSettingsAction = () => ({ type: settingsTypes.GET_SETTINGS });
export const setPersonalColorAction = payload => ({ type: settingsTypes.SET_PERSONAL_COLOR, payload });
export const setSettingsErrorAction = payload => ({ type: settingsTypes.SET_SETTINGS_ERROR, payload });
export const flushSettingsErrorAction = () => ({ type: settingsTypes.FLUSH_SETTINGS_ERROR });

