import { allowedColors } from '../../styles/colors';
import { reducer, createActionTypes } from '../reduxUtils';

export const settingsTypes = createActionTypes('settings', [
  'GET_SETTINGS',
  'SET_PERSONAL_COLOR',
  'SET_SETTINGS_ERROR',
  'FLUSH_SETTINGS_ERROR'
]);

const initialState = {
  personalColor: allowedColors.aquamarine,
  error: false,
  errorResponse: null,
}

export default reducer(initialState, {
  [settingsTypes.SET_PERSONAL_COLOR]: (state, { payload }) => ({
    ...state,
    personalColor: payload,
  }),
  [settingsTypes.SET_SETTINGS_ERROR]: (state, { payload }) => ({
    ...state,
    error: true,
    errorResponse: payload,
  }),
  [settingsTypes.FLUSH_SETTINGS_ERROR]: state => ({
    ...state,
    error: false,
    errorResponse: null,
  }),
});
