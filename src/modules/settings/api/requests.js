import ApiGateway from "../../apiGateway";
import { getSettingsQuery } from "./queries";

export const getSettingsRequest = () => new ApiGateway().request(getSettingsQuery());