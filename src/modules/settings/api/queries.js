import ApiQuery from "../../apiQuery"

export const getSettingsQuery = () => {
  const query = new ApiQuery();

  query.path(['settings']);

  return query.stringify();
}