import { all, call } from 'redux-saga/effects';
import settingsWatcher from './settings/workers';

export default function* rootSaga() {
  yield all([
    call(settingsWatcher),
  ]);
}