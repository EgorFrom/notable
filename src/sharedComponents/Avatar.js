// @flow
import React, { PureComponent } from 'react';
import {
  View, StyleSheet, Text, SafeAreaView, StatusBar,
} from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../styles/colors';
import { iconSize } from '../styles/spaces';

const imageSize = 100;

class Avatar extends PureComponent {

  getPlaceholder = () => {
    const { name, presonalColor } = this.props;
    const label = name.split(' ').slice(0,2).reduce((acum, curr) => acum+curr[0].toUpperCase(), '');
    return (
      <View style={[styles.placeholderContainer, { backgroundColor: presonalColor }]}>
        <Text style={styles.placeholderText}>{label}</Text>
      </View>
    );
  }

  getImage = image => (
    <FastImage
      style={styles.image}
      source={{ uri: image }}
    />
  );

  render() {
    const { image } = this.props;
    return image ? this.getImage(image) : this.getPlaceholder();
  }
}

const styles = StyleSheet.create({
  image: {
    width: imageSize,
    height: imageSize,
    borderRadius: 25,
  },
  placeholderContainer: {
    width: imageSize,
    height: imageSize,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
  },
  placeholderText: {
    color: colors.white,
    fontSize: imageSize / 2,
    textShadowColor: 'rgba(0, 0, 0, 0.7)',
    // textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 1,
  },
});

export default Avatar;
