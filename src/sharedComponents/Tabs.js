// @flow
import React, { PureComponent } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../styles/colors';

const { width } = Dimensions.get('window');

class Tabs extends PureComponent {
  render() {
    const { tabs = [], onPress, selected } = this.props;
    const tabWidth = tabs.length && width / tabs.length;
    return (
      <View style={styles.container}>
        {tabs.map((tab, index) => (
          <TouchableRipple
            key={`${index}_tab`}
            style={[styles.tabContainer, { width: tabWidth }, selected === index && styles.selectedTab]}
            onPress={() => onPress(index)}
          >
            <Text style={styles.tabText}>{tab}</Text>
          </TouchableRipple>
        ))}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  tabContainer: {
    backgroundColor: colors.primary, 
    padding: 8,
    borderRadius: 5,
  },
  selectedTab: {
    backgroundColor: colors.dark, 
  },
  tabText: {
    color: colors.white,
    textAlign: 'center',
    fontSize: 18,
  },
});

export default Tabs;
