// @flow
import React, { PureComponent } from 'react';
import { View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../styles/colors';

class Emoji extends PureComponent {
  getIconName = mood => {
    switch (mood) {
      case 1:
        return 'sentiment-very-dissatisfied';
      case 2:
        return 'sentiment-dissatisfied';
      case 3:
        return 'sentiment-satisfied';
      case 4:
        return 'sentiment-satisfied-alt';
      case 5:
        return 'sentiment-very-satisfied';
      default:
        break;
    }
  }

  render() {
    const { mood } = this.props;
    return (
      <View>
        <Icon name={this.getIconName(mood)} size={28} color={colors.accent} />
      </View>
    );
  }
}

export default Emoji;
