// @flow
import React, { PureComponent } from 'react';
import {
  View, StyleSheet, Text, SafeAreaView, StatusBar,
} from 'react-native';
import { Appbar } from 'react-native-paper';
import colors from '../styles/colors';

class AppBar extends PureComponent {
  render() {
    const { navigation, screenName } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle={"light-content"} />
        <Appbar.Header style={{backgroundColor: colors.primary}}>
          {/* <Appbar.BackAction onPress={() => navigation.goBack()} /> */}
          <Appbar.Content title={screenName} />
        </Appbar.Header>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
  }
});

export default AppBar;
