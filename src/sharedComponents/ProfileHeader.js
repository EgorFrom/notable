// @flow
import React, { PureComponent } from 'react';
import {
  View, StyleSheet, Text, SafeAreaView, StatusBar,
} from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../styles/colors';
import { iconSize } from '../styles/spaces';
import Avatar from './Avatar';

const imageSize = 100;

class ProfileHeader extends PureComponent {
  render() {
    const { navigation, user: { presonalColor, name, image } } = this.props;
    return (
      <SafeAreaView style={styles.container}>
        {/* <TouchableRipple style={styles.iconContainer} onPress={() => navigation.goBack()}>
          <Icon name={'arrow-back-ios'} size={iconSize} color={colors.white} />
        </TouchableRipple> */}
        <View style={styles.inlineBlock}>
          <Avatar image={image} name={name} presonalColor={presonalColor} />
          <View style={styles.titleContainer}>
            <Text numberOfLines={2} style={styles.titleText}>{name}</Text>
          </View>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.primary,
    marginTop: 0,
    margin: 16,
  },
  iconContainer: {
    width: iconSize,
    height: iconSize,
    marginVertical: 16,
    borderRadius: 25,
    paddingLeft: 4,
    transform: [{ translateX: -4 }]
  },
  image: {
    width: imageSize,
    height: imageSize,
    borderRadius: 25,
  },
  inlineBlock: {
    flexDirection: 'row',
  },
  titleText: {
    fontSize: 32,
    color: colors.white,
  },
  titleContainer: {
    flex: 1,
    margin: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default ProfileHeader;
