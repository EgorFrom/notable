import React, { PureComponent } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import screens from './screens';

const MessengerStack = createStackNavigator();

class MessengerStackScreen extends PureComponent {
  render() {
    return (
      <MessengerStack.Navigator>
        {screens.map(screen => (
          <MessengerStack.Screen
            key={screen.name}
            name={screen.name}
            component={screen.component}
            options={screen.options}
          />
        ))}
      </MessengerStack.Navigator>
    );
  }
}

export default MessengerStackScreen;