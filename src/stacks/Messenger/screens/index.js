import Messenger from "./Messenger";
import { defaultNavigator } from "../../../styles/navigation"

export default [
  { component: Messenger, name: 'Messenger', options: defaultNavigator },
];
