import React, { PureComponent } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import screens from './screens';

const ReportsStack = createStackNavigator();

class ReportsStackScreen extends PureComponent {
  render() {
    return (
      <ReportsStack.Navigator>
        {screens.map(screen => (
          <ReportsStack.Screen
            key={screen.name}
            name={screen.name}
            component={screen.component}
            options={screen.options}
          />
        ))}
      </ReportsStack.Navigator>
    );
  }
}

export default ReportsStackScreen;