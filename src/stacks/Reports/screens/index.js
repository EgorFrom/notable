import Reports from "./Reports";
import { defaultNavigator } from "../../../styles/navigation"

export default [
  { component: Reports, name: 'Reports', options: defaultNavigator },
];
