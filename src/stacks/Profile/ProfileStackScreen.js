import React, { PureComponent } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import screens from './screens';

const ProfileStack = createStackNavigator();

class ProfileStackScreen extends PureComponent {
  render() {
    return (
      <ProfileStack.Navigator>
        {screens.map(screen => (
          <ProfileStack.Screen
            key={screen.name}
            name={screen.name}
            component={screen.component}
            options={screen.options}
          />
        ))}
      </ProfileStack.Navigator>
    );
  }
}

export default ProfileStackScreen;