import Profile from "./Profile";
import { defaultNavigator } from "../../../styles/navigation"

export default [
  { component: Profile, name: 'Profile', options: defaultNavigator },
];
