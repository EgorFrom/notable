import Settings from "./Settings";
import { defaultNavigator } from "../../../styles/navigation"

export default [
  { component: Settings, name: 'Settings', options: defaultNavigator },
];
