import React, { PureComponent } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import screens from './screens';

const SettingsStack = createStackNavigator();

class SettingsStackScreen extends PureComponent {
  render() {
    return (
      <SettingsStack.Navigator>
        {screens.map(screen => (
          <SettingsStack.Screen
            key={screen.name}
            name={screen.name}
            component={screen.component}
            options={screen.options}
          />
        ))}
      </SettingsStack.Navigator>
    );
  }
}

export default SettingsStackScreen;