// @flow
import React, { PureComponent } from 'react';
import {
  View, StyleSheet, Text, SafeAreaView, Button,
} from 'react-native';
import { connect } from 'react-redux';
import { List } from 'react-native-paper';
import { teamsScreens } from '.';
import colors, { allowedColors } from '../../../styles/colors';
import Emoji from '@/sharedComponents/Emoji';
import { getSettingsAction } from '../../../modules/settings/actions';

const mockTeam = {
  name: 'BrawlParty',
  members: [
    { id: 1, name: 'Egor many text here may be writted many text here may be writted', mood: 1, presonalColor: allowedColors.aquamarine },
    { id: 2, name: 'Nikita Vadimovich', mood: 2, presonalColor: allowedColors.darkLiver },
    { id: 3, name: 'Olga', mood: 3, presonalColor: allowedColors.cooperRose, image: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEA8TEg8TFRMXFQ8VFRUVFRASFRUSFREWFhUXGBUYHiggGBolHRUVIj0hJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGxAQGjUlHyUuMzU3Ky0xNy0uMS83Ny8xLjU3MDcvMy0wNTM3Ni0yLTItNS0rNjU3LTc1NysuLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAAAQIEBgcFAwj/xABBEAACAQICBQYLBwMEAwAAAAAAAQIDEQQSBQYHITETIjVBYbMUNFFTcXN0kZKx0TJScoGDobIkQsM2YmOCFRYj/8QAGgEBAAMBAQEAAAAAAAAAAAAAAAECAwQGBf/EAC8RAQABAwEHAwIFBQAAAAAAAAABAgMRMQUSEyFBUZEEUmFx0RSBscHhFSIjMqH/2gAMAwEAAhEDEQA/AO1AAAERdwJAAAAAAAAAAAAAAAAAPk8RBcZxXY2k/cyMwtFFVXOIR4VT85D4o/UZhbhV+2fB4VT85D4o/UZg4VftnweFU/OQ+KP1G9COFX7Z8HhVPzkPij9RmDhV+2fB4VT85D4o/UZg4VftnweFU/OQ+KP1GYOFX7Z8HhVPzkPij9RmE8Kv2z4fVMlmkAAApKQFogSAAAAAFWwFgJTAkAAAAAAFGwND1hwClpKEZXtVdC9uNnaDt8Jx3ac3Md3qdn+omnZ81U60733/AHed4BTlVrxjwhRqzWWaqXnFXV5ZV7rfmU3YzOOzs/EXabdFVWs1RHOMcp+Mz5W0BomNd1M90koxVssf/pNtRvfilZ7uIt24qzlHr/W1WIp3dZ59+Uavlg8LSdHESqU556eThOMVec8lmsr4O/Xv7BTFO7Oei967d4tFNFUbtWemdIz36vjo7CxqOopX5tKtNWsudCDa/IrTETn6NvUXarcUzHWqI/KZfSthKcKFKThOU6kZyUk0oQyzcbWyvM92/euKJmmIpiWVF65XfqpiYiKZiMdZ5Z78vjVmf+Jp5GkpuoqEa+bdkea3My27bJ34p9RfcjHzjLm/G3JrzmN3f3cdfrr55aPpPQlPkeUTalGgqk192cmsjt912mv+o4cbufhSn113jbmsTViPpGv56Ny1aX9Jh/wI6rf+kPP7QnPqa5+Xpl3GAUlICYxAsAAAAAACqAkAkBIAAAAAUkwJigPE1g0A8RUpVIVuTlBWvZt8bxaaaaad/eZXLe9MTEvp+h2hHp6KqKqN6JeVR1NqwlmhjMst/OjGcXv471K5nFiY0l3V7atVxu1Wsx2mY+xLU2s73xjd5Kb5s3ea4S+19rt4jgT7kRtmzGlnpjWNO2mnwh6l1ed/V/a3y5s+c73vLnc7fv3jgT3W/rdrl/i005xy+nLkiGpNRXtikrpp2hJXTVmnaXB+QiPTz3TVt23Vra/7/C0dTayi4LGNQfGKU1F+mOazJ4E4xvKTtmzNUVzZ5x1zGfOH2paq1lu8LvD7jU8qfG6jmsnfsJ4M92c7Wszz4X93fln9MpnqpOTbeJdnFQss6Tgnez52+N+rgODPcja1EaW+uemvhsWjcLyVKnTvfLFK9rX/AC6jamMRh8i/d4tyqvvLJLMkSQERiBYAAAAAAACGgCQEgAAAABWaugEUBYAAAAAAACGgCQEgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA1bWfXrDYKfJNSq1lZuELcy6us8nuTfkV3w3b0BjavbRcLiakaUoTozk0oZ3GUJSfCOdcG+1K/DiBuQAAAAAAAAAAAAAAAAAAAAAAAAAAAAERdwJA/NGMrTnUqTqXzynOU78czk3L9wPiwP0foOtOeFw06n25UaMp/idNN/uBnAAAAAAAAAAAABEXdASAAq2At6QJTAkAAAAAAFJMC0UBNgOda4bNpVq06+FnCMptynTnmjHO+MoySdrvfZri3v6gMLV7ZbUVSM8ZOGSLT5Km5Sc7dUpNJKPovfsA6lbsAAVYCwEpgSAAAAAACjYFogSAAqBIBICQAAAAAo3cCYxA13XjWlYChFxSlWqZlSi+G62acv9qut3W2vSg4tpLTmJxEnKtiKk2+rM1FdkYLmxXoQGHy8/vy+KQDl5/fl8UgMjB6Ur0pKVKvVhJdcZyXvV7NdjA69s91xeNjKlWssRBZrrcqkL2zJdUk2rrhvTXkQbjECQCQEgAAACGwKN3AtFAWAAAIaAJASAAAAAESQERQFgOSbZfGsKv8Ahl3jA1OGgajwMsbnhycaqpOPOz5ubv4Wtzl1geSAA9bA6BqVcJicVGcFCi4qUXmzPNltbdb+5dYHs7K+k6Xq6/dsDtrQBICQAAAAAiSAiKAsAAAAAAAAAAAAAAAA5Htm8awvqX3kgMahv1cre1x/xgaOAA3fVvoPS/46PzpgY2yzpOl+Cv3bA7eAAAAAAAAAAAAAAAAAAAAAAAAAAHI9s3jWF9S+9kBi0P8ATlb2uP8AjA0rj6fmAsBuurnQemPx0fnTAxtlfSdL8Ffu2B28AAAAAAAAAAAAAAAAAAAAAAAAAAOSbZ4vwnCu250ZJPtVR3+a94GLRi//AFuru44tNdqzQXzT9wGkATx9PzA3bVqLeg9MWX99J/kuTb/ZMDG2VRb0nS3cKddvsWS1/e17wO3AAAAAAAAAAAAAAAAAAAAAAAAAABh6S0XQxEVGvRhUindKaTs/Kn1AavtNoQp6KlCnCMYRnh1GMUoxSz9SQHJ9XaMamMwkJxUoyrUIyT4OMqiTT/ID0NfsFTo6QxFOlBQpx5K0Y8FejBv92wN42NpSw2MTSadWKaaumnSV011gbro3Q2Gw7k6GHp03L7ThFRbXUr+Ts4AZ4AAAAAAAAAAAAAAAABEXcCQAAAAAAAAGn7V+jKnrKH8wOT6q+P4H2jD97ED09pnSmK/Q7iAG47F/F8X66HdoDogAABEXdcAJAAAAAAAAAAAAD5uQF4oCQAAAAAq2AygSmBqG1foyp6yh/MDk+qvj+B9ow/exA9PaZ0piv0O4gBuOxfxfF+uh3aA6IBDAo2BdASAAAAAFeICwEpgSAA+blcC0UBYAAAAAAFUBIBIDUNq/RlT1lD+YHJ9VfH8D7Rh+9iB6e0zpTFfodxADcdi/i+L9dDu0B0NsCjdwLRQFgAAAAAAViBIBICQIaAiMfKBYAAAAAAACGgCQEgaftX6MqesofzA5Pqr4/gfaMP3sQPT2mdKYr9DuIAbjsX8Xxfrod2gOhyVwIS94FgAAAAAAAIaAJASAAAAAAAAAAAAAAAA83WLQ8MZhqtCbcVK1pLe4yi04yt171w61cDTdWNmrw+Jp1q2IjNU5ZoQhGSzTX2XJvgk99l1pb/KGXrrqA8ZX5elWjTm4xjOM03GWVWUk1vTtZW67Lh1h7mp+rkcBh3SU885Sc6k7Zbyskkl1JJL9/KB7oAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABDAq2/IBRykBRzkBVzkBV1JgUVWYEOpMCOUmA5SYE8pMCyqTAsqkwLKcgLKcgLqUgLJvyAWVwLAAAFZP3gSgJAAVYDKBKYEgAAAABDAo2BeKAWAWAWAWAWAjiAygSmBIAAAAARJgUSA+gAABUCQCQEgAAACGwKtgTFAWAAAAAABVASASAkAAAARJgUW8C6QEgAAENAEgJAAAAACGgIjECwAAAAAAAENAEgJAAAAACJK4BICQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAH/2Q==' },
    { id: 4, name: 'Zahar', mood: 4, presonalColor: allowedColors.skobeloff, chores: ['wash', 'clean', 'eat', 'run', 'walk', 'talk', 'look', 'learn', 'review', 'wash', 'clean', 'eat', 'run', 'walk', 'talk', 'look', 'learn', 'review']},
    { id: 5, name: 'Ilya', mood: 5, presonalColor: allowedColors.periwinkleCrayola },
    { id: 6, name: 'Sasha', mood: 5, presonalColor: allowedColors.mistyMoss },
  ],
};

class Teams extends PureComponent {
  state = {
    expanded: true,
  };

  render() {
    const { navigation } = this.props;
    const { expanded } = this.state;
    return (
      <SafeAreaView>
        <List.Accordion
          title={mockTeam.name}
          expanded={expanded}
          onPress={() => this.setState({ expanded: !expanded })}>
          {mockTeam.members.map(person => (
            <List.Item
              key={person.name}
              title={person.name}
              right={() => <Emoji mood={person.mood} />}
              onPress={() => navigation.navigate(teamsScreens.TeamMember, { person })}
            />
          ))}
        </List.Accordion>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
});

const mapStateToProps = state => ({
  // personalColor: state.settings.personalColor,
});

const mapDispatchToProps = dispatch => ({
  // setPersonalColor: color => dispatch(changePersonalColor(color)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Teams);
