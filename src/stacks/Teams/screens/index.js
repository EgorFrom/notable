import Teams from "./Teams";
import TeamMember from "./TeamMember";
import { defaultNavigator, hiddenNavigator } from "../../../styles/navigation"

export const teamsScreens = {
  Teams: 'Teams',
  TeamMember: 'TeamMember',
}

export default [
  { component: Teams, name: teamsScreens.Teams, options: defaultNavigator },
  { component: TeamMember, name: teamsScreens.TeamMember, options: hiddenNavigator },
];
