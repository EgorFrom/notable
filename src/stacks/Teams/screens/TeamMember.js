// @flow
import React, { PureComponent } from 'react';
import {
  View, StyleSheet, Text, SafeAreaView, Button, Dimensions, FlatList,
} from 'react-native';
import { LineChart } from 'react-native-charts-wrapper';
import { TouchableRipple, List } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import colors from '../../../styles/colors';
import ProfileHeader from '../../../sharedComponents/ProfileHeader';
import { iconSize } from '../../../styles/spaces';
import Tabs from '../../../sharedComponents/Tabs';

const { width, height } = Dimensions.get('window');
// '01-01-2021': 4
// `${i}-01-2021`
const getArray = () => {
  const resp = [];
  for(let i = 1; i <= 31; i++) {
    resp.push({ x: i, y: parseInt(Math.floor(Math.random() * Math.floor(5)))+1 });
  }
  return resp;
}

class TeamMember extends PureComponent {
  state = {
    selectedTab: 0,
  };

  renderStatistics = data => (
    <View style={{padding: 16}}>
      <LineChart
        xAxis={{
          granularity: 1,
          position: 'BOTTOM',
        }}
        yAxis={{
          left: {
            granularity: 1,
          },
          right: {
            granularity: 1,
          }
        }}
        style={styles.chart}
        data={{dataSets:[{label: "emotion", values: data }]}}
        visibleRange={{
          x: { min: 1, max: 5 },
          y: { min: 1, max: 5 }
        }}
      />
    </View>
  )

  renderChores = (chores) => (
    <FlatList
      keyExtractor={(item, index) => `chore_${item}_${index}`}
      data={chores}
      renderItem={({ item }) => (
        <View style={{padding: 16}}><Text>{item}</Text></View>
      )}
    />
  );

  render() {
    const { navigation, route: { params: { person } } } = this.props;
    const { selectedTab } = this.state;
    const data = getArray();
    return (
      <SafeAreaView style={styles.container}>
        <View >
          <TouchableRipple style={styles.iconContainer} onPress={() => navigation.goBack()}>
            <Icon name={'arrow-back-ios'} size={iconSize} color={colors.white} />
          </TouchableRipple>
          <ProfileHeader navigation={navigation} user={person}/>
          <Tabs
            tabs={['chores', 'statistics', 'tests']}
            onPress={index => this.setState({ selectedTab: index })}
            selected={selectedTab}
          />
        </View>
        <View style={styles.body}>
          {selectedTab === 0 && person.chores && this.renderChores(person.chores)}
          {selectedTab === 1 && this.renderStatistics(data)}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  iconContainer: {
    width: iconSize,
    height: iconSize,
    marginVertical: 16,
    borderRadius: 25,
    paddingLeft: 4,
    transform: [{ translateX: -4 }],
    marginLeft: 16,
  },
  chart: {
    width: width - 32,
    height: 300,
  },
  body: {
    flex: 1,
    backgroundColor: colors.background,
  },
});

export default TeamMember;
