import React, { PureComponent } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import screens from './screens';

const TeamsStack = createStackNavigator();

class TeamsStackScreen extends PureComponent {
  render() {
    return (
      <TeamsStack.Navigator>
        {screens.map(screen => (
          <TeamsStack.Screen
            key={screen.name}
            name={screen.name}
            component={screen.component}
            options={screen.options}
          />
        ))}
      </TeamsStack.Navigator>
    );
  }
}

export default TeamsStackScreen;